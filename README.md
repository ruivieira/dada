# dada

Artisan Rust documentation (inspired by `docco`).

Provide a input source file and an output destination for the document.

```
dada -i foo.rs -o foo.html
```

An example of `dada` running against its own source is available [here](https://ruivieira.gitlab.io/dada/).